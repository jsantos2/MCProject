package insure.sise.grupo7.insureproject;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;

public class NotificationHelper extends ContextWrapper{

    private static final String INSURE_CHANNEL_ID = "insure.channel";
    private static final String INSURE_CHANNEL_NAME = "InSure Channel";
    private NotificationManager manager;
    public NotificationHelper(Context context) {
        super(context);
        createChannels();
    }

    private void createChannels() {
        NotificationChannel insureChannel = new NotificationChannel(INSURE_CHANNEL_ID, INSURE_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        insureChannel.enableLights(true);
        insureChannel.enableVibration(true);
        insureChannel.setLightColor(Color.GREEN);
        insureChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        getManager().createNotificationChannel(insureChannel);
    }
    public NotificationManager getManager() {
            if(manager == null) {
                manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            }
            return manager;
    }
    public Notification.Builder getInsureChannelNotification(String title, String body) {
        return new Notification.Builder(getApplicationContext(), INSURE_CHANNEL_ID)
                .setContentText(body)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.insure_launcher_icon_round)
                .setAutoCancel(true);
    }
}

