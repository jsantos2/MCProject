package insure.sise.grupo7.insureproject.WebServer;

import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import insure.sise.grupo7.insureproject.GlobalState;

public class WSSubmitNewMessageCallTask extends AsyncTask<Void, String, Void> {
    public final static String TAG = "NewMessageCallTask";
    private int _sessionId;
    private int _claimId;
    private String _message;

    public WSSubmitNewMessageCallTask(int sessionId, int claimId, String message) {
        _sessionId = sessionId;
        _claimId = claimId;
        _message = message;
    }

    @Override
    protected Void doInBackground(Void... params) {
        /*
         * Test method call invocation: submitNewMessage
         */
        publishProgress("Testing method call submitNewMessage...");
        try {
            int claimId = GlobalState.getClaimId();
            boolean r = WSHelper.submitNewMessage(_sessionId, claimId, _message);
            Log.d(TAG, "Submit New Message claimId " + claimId + " message:" + _message + " result => " + r);
            publishProgress("ok.\n");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
        }
        return null;
    }

    protected void onPostExecute() {
       // if () {
            //submit message
            // build successful alert dialog
       // }
       // else {
            // failed, offline mode
        //}
    }
}
