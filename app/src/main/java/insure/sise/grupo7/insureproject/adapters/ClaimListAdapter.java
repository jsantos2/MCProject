package insure.sise.grupo7.insureproject.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.datamodel.ClaimItemStatus;

public class ClaimListAdapter extends ArrayAdapter<ClaimItemStatus> {
    private Context _context;
    private List<ClaimItemStatus> _listOfClaims;
    int _resource;

    public ClaimListAdapter(Context context, int resource, List<ClaimItemStatus> listOfClaims){
        super(context, resource, listOfClaims);
        _listOfClaims = listOfClaims;
        _context = context;
        _resource = resource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        int claimnr = getItem(position).getId();
        String claimTitulo = getItem(position).getTitle();
        String claimnumero = String.valueOf(claimnr);
        int _statusResource = getItem(position).getStatusResource();
        LayoutInflater inflater = LayoutInflater.from(_context);
        convertView = inflater.inflate(_resource, parent, false);

        ImageView claimStatusIcon = (ImageView) convertView.findViewById(R.id.claimStatusImg);
        claimStatusIcon.setTag(new Integer(position));
        claimStatusIcon.setImageResource(_statusResource);

        TextView claimNumber = (TextView) convertView.findViewById(R.id.claimNumber);
        TextView claimTitle = (TextView) convertView.findViewById(R.id.claimTitle);
        claimNumber.setText(claimnumero);
        claimTitle.setText(claimTitulo);

        return convertView;
    }
}
