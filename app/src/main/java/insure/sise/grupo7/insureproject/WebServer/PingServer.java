package insure.sise.grupo7.insureproject.WebServer;

import android.os.AsyncTask;
import android.util.Log;

import insure.sise.grupo7.insureproject.GlobalState;

public class PingServer extends AsyncTask<Void, String, Void> {
    String TAG = "Test";

    @Override
    protected Void doInBackground(Void... params) {
        publishProgress("Testing method call hello...");
        try {
            String r = WSHelper.hello("John Doe");
            publishProgress("ok.\n");
            GlobalState.disableOfflineMode();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
        }
    return null;
    }
}