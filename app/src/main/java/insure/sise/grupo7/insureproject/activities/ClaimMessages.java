package insure.sise.grupo7.insureproject.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import androidx.work.BackoffPolicy;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import insure.sise.grupo7.insureproject.AlertDialogs.OfflineAlertDialogHelper;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.WebServer.WSListClaimMessagesCallTask;
import insure.sise.grupo7.insureproject.WebServer.WSSubmitNewMessageCallTask;
import insure.sise.grupo7.insureproject.Workers.MessagesWorker;
import insure.sise.grupo7.insureproject.adapters.ChatMessageAdapter;
import insure.sise.grupo7.insureproject.datamodel.Message;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;


public class ClaimMessages extends BaseActivity {

    private EditText editText;
    private String _message;
    private ChatMessageAdapter messageAdapter;
    private ListView _messagesView;

    private static final String TAG = "ClaimMessages";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_messages);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        super.prepareMenu(toolbar, "ClaimMessages");

        editText = (EditText) findViewById(R.id.editText);
        if (GlobalState.getOfflineState()) {
            OfflineAlertDialogHelper.buildAlert(this, "CLAIMCHAT");
        }
        getMessages();

        startRefreshWorker();

    }

    private void startRefreshWorker() {
        GlobalState.setContext(this);
        OneTimeWorkRequest messageWorker =
                new OneTimeWorkRequest.Builder(MessagesWorker.class)
                        .setBackoffCriteria(BackoffPolicy.LINEAR, 3, TimeUnit.SECONDS)
                        .build();
        WorkManager.getInstance().beginUniqueWork("RefreshWorker", ExistingWorkPolicy.REPLACE, messageWorker).enqueue();
    }


    protected void onPause() {
        super.onPause();
        GlobalState.setDestroyMessageWorker(true);
    }


    protected void onStop() {
        super.onStop();
        GlobalState.setDestroyMessageWorker(true);
    }

    protected void onStart() {
        super.onStart();
        GlobalState.setDestroyMessageWorker(false);
        getMessages();
        startRefreshWorker();
    }

    protected void onResume() {
        super.onResume();
        GlobalState.setDestroyMessageWorker(false);
        getMessages();
        startRefreshWorker();
    }

        public void sendMessage (View view){
            editText = (EditText) this.findViewById(R.id.editTextMessage);
            String message = editText.getText().toString();
            Log.d(TAG, "sendMessage");
            if (message.length() > 0) {
                new WSSubmitNewMessageCallTask(GlobalState.getSessionId(),GlobalState.getClaimId(), message).execute();
                getMessages();

                editText.getText().clear();
            }
        }

        public void getMessages () {
            Log.d(TAG, "getMessages");
            new WSListClaimMessagesCallTask(this).execute();
        }

        public ArrayList<Message> getMessagesList (String[]messagesArray){
            Log.d(TAG, "getMessagesList");
            ArrayList<Message> messagesList = new ArrayList<Message>();
            for (int i = 1; i < messagesArray.length; i++) {
                messagesList.add(new Message(messagesArray[i]));
            }
            return messagesList;
        }

        public void setMessage (String message){
            this._message = message;
            String[] messagesArray = _message.split("\\(Sender");
            _messagesView = (ListView) findViewById(R.id.messages_view);
             messageAdapter = new ChatMessageAdapter(this, 1, getMessagesList(messagesArray));
            _messagesView.setAdapter(messageAdapter);
            _messagesView.setSelection(_messagesView.getAdapter().getCount()-1);
        }

    public void offlineAlertDialog() {
        GlobalState.enableOfflineMode();
        OfflineAlertDialogHelper.buildAlert(this, "CLAIMDETAILS");
    }

    }



