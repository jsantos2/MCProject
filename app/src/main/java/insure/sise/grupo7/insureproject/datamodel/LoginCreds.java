package insure.sise.grupo7.insureproject.datamodel;

import java.io.Serializable;

public class LoginCreds implements Serializable{

    String _username;
    String _password;

    public LoginCreds(String username, String password) {
        _username = username;
        _password = password;
    }

    public String get_username() {
        return _username;
    }

    public void set_username(String _username) {
        this._username = _username;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        this._password = password;
    }
}
