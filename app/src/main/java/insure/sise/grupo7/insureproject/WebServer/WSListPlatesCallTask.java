package insure.sise.grupo7.insureproject.WebServer;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.LicensePlatesListPopUp;
import insure.sise.grupo7.insureproject.activities.ClaimSubmitActivity;

public class WSListPlatesCallTask extends AsyncTask<Void, String, List<String>> {
    public final static String TAG = "LoginTask";
    int _sessionId;
    private ClaimSubmitActivity _claimSubmitActivity;
    List<String> _plateList = new ArrayList<>();

    public WSListPlatesCallTask(ClaimSubmitActivity claimSubmitActivity, int sessionID) {
        this._sessionId = sessionID;
        _claimSubmitActivity = claimSubmitActivity;
    }

    @Override
    protected List<String> doInBackground(Void... params) {
        publishProgress("Testing method call listPlates...");
        try {
            List<String> plateList = WSHelper.listPlates(_sessionId);
            if (plateList != null) {
                _plateList = plateList;
                String m = plateList.size() > 0 ? "" : "empty array";
                for (String plate : plateList) {
                    m += " (" + plate + ")";
                }
                Log.d(TAG, "List plates result => " + m);
            } else {
                Log.d(TAG, "List plates result => null.");
            }
            publishProgress("ok.\n");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
        }

        return _plateList;
    }

    @Override
    protected void onPostExecute(List<String> plateList) {
        if (plateList.size() > 0 ) {
            try {
                String _username = GlobalState.getUsername();
                InternalStorage.writeObject(_claimSubmitActivity, "PlateList" + "_username_" + _username, plateList);
                LicensePlatesListPopUp _licensePlatesListPopUp = new LicensePlatesListPopUp(_claimSubmitActivity, plateList);
                _licensePlatesListPopUp.buildLicensePlatesDialogBox(_claimSubmitActivity);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            _claimSubmitActivity.offlineAlertDialogPlates();
        }
    }
}
