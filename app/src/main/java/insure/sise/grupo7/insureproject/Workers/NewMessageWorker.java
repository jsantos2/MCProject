package insure.sise.grupo7.insureproject.Workers;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidx.work.Worker;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.NotificationHelper;
import insure.sise.grupo7.insureproject.WebServer.WSHelper;
import insure.sise.grupo7.insureproject.activities.ClaimMessages;
import insure.sise.grupo7.insureproject.datamodel.ClaimMessage;

public class NewMessageWorker extends Worker {
    List<ClaimMessage> claimMessages = new ArrayList<>();
    int _insureMessagesCounter = 0;
    String TAG = "NewMessageWorker";
    int sessionId;
    int claimId;
    @Override
    public Worker.Result doWork() {
        NotificationHelper helper = new NotificationHelper(getApplicationContext());
        if (GlobalState.isLoggedOutState()) {
            return Result.FAILURE;
        } else if (GlobalState.getClaimId() == 0) {
            return Result.RETRY;
        }
        Log.d(TAG, "New message worker is working.");
        try {
            claimMessages = WSHelper.listClaimMessages(GlobalState.getSessionId(), GlobalState.getClaimId());
            for (ClaimMessage claimMessage : claimMessages) {
                if (claimMessage.getSender().equals("AutoInSure")) {
                    _insureMessagesCounter++;
                }
            }
            Log.d("counter global state", String.valueOf(GlobalState.getMessagesCounter()));
            Log.d("counter worker", String.valueOf(_insureMessagesCounter));
            if (_insureMessagesCounter > GlobalState.getMessagesCounter()) {
                Notification.Builder builder = helper.getInsureChannelNotification("Insure Notification", "You have a new message from InSure.");
                helper.getManager().notify(new Random().nextInt(), builder.build());
            }
        } catch (Exception e) {
            Log.d(TAG, "Could not get messages - need claim Id");
        }

        // Indicate success or failure with your return value:
        return Result.RETRY;

        // (Returning RETRY tells WorkManager to try this task again
        // later; FAILURE says not to try again.)
    }
}