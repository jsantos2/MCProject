package insure.sise.grupo7.insureproject.WebServer;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.activities.MyClaimActivity;
import insure.sise.grupo7.insureproject.datamodel.ClaimItem;
import insure.sise.grupo7.insureproject.datamodel.ClaimItemStatus;
import insure.sise.grupo7.insureproject.datamodel.ClaimRecord;

public class WSListClaimsCallTask extends AsyncTask<Void, String, List<ClaimItemStatus>> {
    public final static String TAG = "ListClaimsTask";
    int _sessionId;
    Context _context;
    MyClaimActivity _myClaimsActivity;
    List<ClaimItemStatus> claimItemStatuses = new ArrayList<>();
    boolean offlineStatus = false;
    public WSListClaimsCallTask(MyClaimActivity myClaimsActivity, Context context, int sessionId) {
        _sessionId = sessionId;
        _context = context;
        _myClaimsActivity = myClaimsActivity;
    }

    @Override
    protected List<ClaimItemStatus> doInBackground(Void... params) {
        String m = "empty array";
        publishProgress("Testing method call listClaims...");
        try {
            List<ClaimItem> claimItemList = WSHelper.listClaims(_sessionId);
            if (claimItemList != null) {
                m = claimItemList.size() > 0 ? "" : "empty array";
                for (ClaimItem claimItem : claimItemList ) {
                    m += " ("+ claimItem.toString() + ")";
                    String claimStatus = getClaimStatus(claimItem.getId(), _sessionId);
                    int resourceId = 0;
                    switch(claimStatus) {
                        case "pending":
                            resourceId = R.drawable.pending;
                            break;
                        case "accepted":
                            resourceId = R.drawable.accepted;
                            break;
                        case "denied":
                            resourceId = R.drawable.denied;
                            break;
                    }
                    claimItemStatuses.add(new ClaimItemStatus(claimItem.getId(), claimItem.getTitle(), resourceId ));
                }
                String _username = GlobalState.getUsername();
                InternalStorage.writeObject(_context, "ClaimList" + "_username_" + _username, claimItemStatuses);
                Log.d(TAG, "List claim item result => " + m);
            } else {
                Log.d(TAG, "List claim item result => null.");
            }
            publishProgress("ok.\n");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
            offlineStatus = true;
        }
        return claimItemStatuses;
    }

    protected void onPostExecute(List<ClaimItemStatus> claimItemStatusList) {
        if (!offlineStatus) {
            _myClaimsActivity.receiveClaimList(claimItemStatuses);
            _myClaimsActivity.listViewHandler();
        }
        else {
            _myClaimsActivity.offlineAlertDialog();
        }
    }

    private String getClaimStatus(int claimId, int sessionId) {
        String claimState = "";
        try {
        ClaimRecord claimRecord = WSHelper.getClaimInfo(sessionId, claimId);
        if (claimRecord != null) {
            claimState = claimRecord.getStatus();
        } else {
            Log.d(TAG, "Get Claim Info result claimId " + claimId + " => null.");
        }
        publishProgress("ok.\n");
    } catch (Exception e) {
        Log.d(TAG, e.toString());
        publishProgress("failed.\n");
    }
    return claimState;
    }
}
