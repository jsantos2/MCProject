package insure.sise.grupo7.insureproject.WebServer;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.activities.PersonalDetails;
import insure.sise.grupo7.insureproject.datamodel.Customer;

public class WSGetCustomerInfoCallTask extends AsyncTask<Void, String, Customer> {
    public final static String TAG = "GetCustomerInfo";
    PersonalDetails _personalDetails;
    Context _context;
    int _sessionId;
    Customer _customer;
    public WSGetCustomerInfoCallTask(PersonalDetails personalDetails, Context context, int sessionId) {
        _context = context;
        _personalDetails = personalDetails;
        _sessionId = sessionId;
    }
    @Override
    protected Customer doInBackground(Void... params) {
        int sessionId = -1;
        publishProgress("Testing method call getCustomerInfo...");
        try {
            Customer customer = WSHelper.getCustomerInfo(_sessionId);
            saveCustomer(customer);
            if (customer == null) {
                Log.d(TAG, "Get customer info result => null");
            } else {
                String _username = GlobalState.getUsername();
                InternalStorage.writeObject(_context, "PersonalDataInfo" + "_username_" + _username, customer);
                Log.d(TAG, "Get customer info result => " + customer.toString());
            }
            publishProgress("ok.\n");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
        }
        return _customer;
    }

    protected void onPostExecute(Customer customer) {
        if (customer != null) {
            _personalDetails.receiveCustomerDetails(customer);
        }
        else {
            _personalDetails.offlineAlertDialog();
        }
    }

    private void saveCustomer(Customer customer) {
        this._customer = customer;
    }
}
