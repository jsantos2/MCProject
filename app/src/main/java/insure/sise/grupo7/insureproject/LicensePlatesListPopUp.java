package insure.sise.grupo7.insureproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import insure.sise.grupo7.insureproject.activities.ClaimSubmitActivity;

public class LicensePlatesListPopUp {

    private static String[] _licensePlatesList;
    private static String _licensePlate;
    private static ClaimSubmitActivity _claimSubmitActivity;
    public LicensePlatesListPopUp(ClaimSubmitActivity claimSubmitActivity, List<String> licensePlatesList){
       platesListToArray(licensePlatesList);
       _claimSubmitActivity = claimSubmitActivity;
    }

    public void platesListToArray(List<String> licensePlatesList){
        _licensePlatesList = new String[licensePlatesList.size()];
        int i = 0;
        for (String licencePlate : licensePlatesList){
            _licensePlatesList[i] = licencePlate;
            i++;
        }

    }

    public static void buildLicensePlatesDialogBox(Context context){
         final AlertDialog.Builder licensePlatesDialog = new AlertDialog.Builder(context);
         licensePlatesDialog.setTitle("Please select the car:");
         licensePlatesDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
             @Override
             public void onDismiss(DialogInterface dialogInterface) {
                 _claimSubmitActivity.setLicensePlate(_licensePlate);
             }
         });
         licensePlatesDialog.setItems(_licensePlatesList, new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int which) {
                 _licensePlate =  _licensePlatesList[which];


             }
         });
        licensePlatesDialog.show();
    }


}


