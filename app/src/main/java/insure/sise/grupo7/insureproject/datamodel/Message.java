package insure.sise.grupo7.insureproject.datamodel;
import android.util.Log;

import insure.sise.grupo7.insureproject.activities.ClaimMessages.*;

public class Message {
    private String name;
    private String date;// data of the user that sent this message
    private String text; // message body


        private boolean belongsToCurrentUser; // is this message sent by us?

        public Message(String  message ) {

            String[] parts = message.split(",", 3);

            this.name = parts[0].split(":")[1];
            this.date = parts[1].split(" ")[3];
            String text_aux = parts[2].split(":")[1];
            this.text =  text_aux.split("\\)")[0];


//            this.belongsToCurrentUser = belongsToCurrentUser;
        }

        public String getName(){ return name; }
        public String getDate(){ return date; }
        public String getText() { return text; }


    public boolean isBelongsToCurrentUser() {
            return belongsToCurrentUser;
        }
    }

