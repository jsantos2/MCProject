package insure.sise.grupo7.insureproject.Workers;

import android.app.Notification;
import android.content.Context;
import android.util.Log;

import java.util.Random;

import androidx.work.Worker;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.NotificationHelper;
import insure.sise.grupo7.insureproject.WebServer.WSHelper;

public class LogoutWorker extends Worker {

    public Worker.Result doWork() {
        String TAG = "LogoutWorker";
        Log.d(TAG, "Logout worker is working");
        try {
            int _sessionId = GlobalState.getWorkerSessionId();
            boolean result = WSHelper.logout(_sessionId);
            if ( result ) {
                NotificationHelper helper = new NotificationHelper(getApplicationContext());
                Notification.Builder builder = helper.getInsureChannelNotification("Insure Notification", "Submit completed successfully.");
                helper.getManager().notify(new Random().nextInt(), builder.build());
                Log.d(TAG, "Logout successful.");
                return Result.SUCCESS;
            }
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
        Log.d(TAG,"Logout failed, retrying...");
        return Result.RETRY;
    }
}
