package insure.sise.grupo7.insureproject.Workers;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.work.Worker;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.WebServer.WSHelper;
import insure.sise.grupo7.insureproject.activities.ClaimMessages;
import insure.sise.grupo7.insureproject.datamodel.ClaimMessage;

import static insure.sise.grupo7.insureproject.WebServer.WSHelper.listClaimMessages;

public class MessagesWorker extends Worker{
    List<ClaimMessage> claimMessages = new ArrayList<>();
    int _insureMessagesCounter = 0;

    public Worker.Result doWork() {
        String TAG = "MessagesWorker";
        Log.d(TAG, "Messages worker is working");
        if ( GlobalState.getDestroyMessageWorker() || GlobalState.getSessionId() == 0) {
            return Result.SUCCESS;
        }
        try {
            int globalMessageCounter = GlobalState.getMessagesCounter();
            claimMessages = WSHelper.listClaimMessages(GlobalState.getSessionId(), GlobalState.getClaimId());
            for (ClaimMessage claimMessage :claimMessages){
                if(claimMessage.getSender().equals("AutoInSure")){
                    _insureMessagesCounter++;
                }
            }
            Log.d("counter global state" , String.valueOf(globalMessageCounter));
            Log.d("counter worker", String.valueOf(_insureMessagesCounter));
            if ( _insureMessagesCounter > globalMessageCounter ) {
                ((ClaimMessages)GlobalState.getContext()).getMessages();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG,"Refreshed messages, retrying...");
        return Worker.Result.RETRY;
    }
}
