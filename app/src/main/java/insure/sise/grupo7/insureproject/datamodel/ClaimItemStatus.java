package insure.sise.grupo7.insureproject.datamodel;

public class ClaimItemStatus extends ClaimItem {
    private int _statusResource;
    public ClaimItemStatus(int claimId, String claimTitle, int statusResource) {
        super(claimId, claimTitle);
        _statusResource = statusResource;
    }

    public int getStatusResource() {
        return this._statusResource;
    }
}

