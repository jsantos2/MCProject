package insure.sise.grupo7.insureproject.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.app.DatePickerDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import insure.sise.grupo7.insureproject.AlertDialogs.NeverLoggedInAlertDialog;
import insure.sise.grupo7.insureproject.AlertDialogs.OfflineAlertDialogHelper;
import insure.sise.grupo7.insureproject.AlertDialogs.SubmissionSuccessfulAlertDialog;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.LicensePlatesListPopUp;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.WebServer.WSListPlatesCallTask;
import insure.sise.grupo7.insureproject.WebServer.WSSubmitNewClaimCallTask;


public class ClaimSubmitActivity extends BaseActivity {

    private String _selectedDate;
    private static final String TAG = "ClaimSubmitActivity";
    private String _licensePlate;
    private String _claimTitle;
    private String _claimDescription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_submit);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        super.prepareMenu(toolbar, "ClaimSubmit");

        final Calendar myCalendar = Calendar.getInstance();

        final EditText editTextDate= (EditText) findViewById(R.id.editText3);
        Button submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newClaimSubmit();
            }
        });
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                private void updateLabel() {
                    String myFormat = "MM/dd/yy"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                    editTextDate.setText(sdf.format(myCalendar.getTime()));
                }

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    _selectedDate = String.format(Locale.ENGLISH, "%d-%d-%d", dayOfMonth, monthOfYear, year);
                    updateLabel();

                }
            };

            editTextDate.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    new DatePickerDialog(ClaimSubmitActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                }
            });

        }


    public void getPlates(View view){
        if ( !GlobalState.getOfflineState() ) {
            new WSListPlatesCallTask(this, GlobalState.getSessionId()).execute();
        } else {
            readFromFile();
        }
    }

    public void neverLoggedInOffline() {
        NeverLoggedInAlertDialog.buildAlert(this, "CLAIM_SUBMIT");
    }

    public void offlineAlertDialogPlates() {
            GlobalState.enableOfflineMode();
            OfflineAlertDialogHelper.buildAlert(this, "CLAIMSUBMIT_PLATES");
        }

    public void readFromFile() {
        List<String> plateList = null;
        try {
            String _username = GlobalState.getUsername();
            plateList = (List<String>) InternalStorage.readObject(this, "PlateList" + "_username_" + _username);
        } catch (IOException |ClassNotFoundException e) {
            e.printStackTrace();
        }
        if ( plateList == null ) {
            this.neverLoggedInOffline();
        } else {
            LicensePlatesListPopUp _licensePlatesListPopUp = new LicensePlatesListPopUp(this, plateList);
            _licensePlatesListPopUp.buildLicensePlatesDialogBox(this);
        }
    }


    public void setLicensePlate(String licensePlate){
        TextView licencePlateText = (TextView) findViewById(R.id.licencePlateSelection);
        licencePlateText.setText(licensePlate);
        _licensePlate = licensePlate;
    }

    public void newClaimSubmit() {
        AlertDialog builder = new AlertDialog.Builder(this).create();
        View view = LayoutInflater.from(this).inflate(R.layout.alertdialog_authentication, null);
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView alertText = (TextView) view.findViewById(R.id.alertText);
        alertText.setText(R.string.submitting);
        builder.setCancelable(false);
        builder.setView(view);
        builder.show();
        getSelectedClaimDetails();
        setGlobalStateNewClaimInfo();
        if (GlobalState.getOfflineState()) {
            GlobalState.startWorkManager("SUBMIT_CLAIM");
            SubmissionSuccessfulAlertDialog.buildAlert(this, builder, "SUBMIT_CLAIM");
        } else {
            new WSSubmitNewClaimCallTask(this, builder, GlobalState.getSessionId(),  _claimTitle, _selectedDate, _licensePlate, _claimDescription).execute();
        }
    }

    private void setGlobalStateNewClaimInfo() {
        GlobalState.setClaimTitle(_claimTitle);
        GlobalState.setOccurenceDate(_selectedDate);
        GlobalState.setPlate(_licensePlate);
        GlobalState.setClaimDescription(_claimDescription);
    }

    private void getSelectedClaimDetails() {
        EditText claimTitle = (EditText) this.findViewById(R.id.newClaimTitle);
        _claimTitle = claimTitle.getText().toString();
        EditText claimDescription = (EditText) this.findViewById(R.id.descriptionText);
        _claimDescription = claimDescription.getText().toString();
    }

    public void offlineAlertDialogSubmit() {
        if ( !GlobalState.getOfflineState() ) {
            GlobalState.enableOfflineMode();
            OfflineAlertDialogHelper.buildAlert(this, "CLAIMSUBMIT");
        }

    }

    public void goToMyHomePage(View view) {
        this.startActivity(new Intent(this, HomePageActivity.class));
    }
}