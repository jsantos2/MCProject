package insure.sise.grupo7.insureproject.WebServer;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.activities.HomePageActivity;
import insure.sise.grupo7.insureproject.activities.Login;
import insure.sise.grupo7.insureproject.datamodel.ClaimRecord;

public class WSLogoutCallTask extends  AsyncTask<Void, String, Boolean>{
        public final static String TAG = "LogoutTask";
        Boolean _result;
        int _sessionId;
        Context _context;
        public WSLogoutCallTask(Context context, Integer sessionId) {
            _sessionId = sessionId;
            _context = context;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            publishProgress("Testing method call logout...");
            try {
                boolean result = WSHelper.logout(_sessionId);
                _result = result;
                Log.d(TAG, "Logout result => " + result);
                publishProgress("    ok.\n");
            } catch (Exception e) {
                Log.d(TAG, e.toString());
                publishProgress("failed.\n");
            }
            return _result;
        }

    protected void onPostExecute(Boolean result) {
        if (result) {
            GlobalState.setLoggedOutState(true);
            _context.startActivity(new Intent(_context, Login.class));
        }
        else {
            // logout failed alert dialog? TO-DO
        }
    }
}
