package insure.sise.grupo7.insureproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.datamodel.Message;

public class ChatMessageAdapter extends ArrayAdapter{
    String TAG = "ChatMessageAdapter";
    Context _context;
    ArrayList<Message> _messageList;

    public ChatMessageAdapter(Context context, int resource, ArrayList<Message> messageList) {
        super(context,resource,messageList);
        this._messageList=messageList;
        this._context = context;

    }

    @Override
    public Message getItem(int i) {
        return _messageList.get(i);
    }

    public View getView(int i, View convertView, ViewGroup viewGroup) {

        LayoutInflater inflater = LayoutInflater.from(_context);

        String text = getItem(i).getText();
        String date = getItem(i).getDate();
        String name = getItem(i).getName();

            if (name.equals("AutoInSure")) {
                convertView = inflater.inflate(R.layout.insure_message, viewGroup, false);
                View avatar = (View)convertView.findViewById(R.id.avatar);
                TextView tvName = (TextView) convertView.findViewById(R.id.sender_name);
                TextView tvDate = (TextView) convertView.findViewById(R.id.message_date);
                TextView tvMessage = (TextView) convertView.findViewById(R.id.message_body);

                tvName.setText(name);
                tvDate.setText(date);
                tvMessage.setText(text);

            }
            else {
                convertView = inflater.inflate(R.layout.client_message, viewGroup, false);

                TextView tvName = (TextView) convertView.findViewById(R.id.sender_name);
                TextView tvDate = (TextView) convertView.findViewById(R.id.message_date);
                TextView tvMessage = (TextView) convertView.findViewById(R.id.message_body);


                tvDate.setText(date);
                tvMessage.setText(text);
            }

        return convertView;
    }
}
