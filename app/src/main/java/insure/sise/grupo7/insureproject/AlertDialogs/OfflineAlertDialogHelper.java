package insure.sise.grupo7.insureproject.AlertDialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import java.util.concurrent.TimeUnit;

import androidx.work.BackoffPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.WebServer.PingServer;
import insure.sise.grupo7.insureproject.Workers.RetryConnectionWorker;
import insure.sise.grupo7.insureproject.activities.ClaimDetails;
import insure.sise.grupo7.insureproject.activities.ClaimSubmitActivity;
import insure.sise.grupo7.insureproject.activities.HomePageActivity;
import insure.sise.grupo7.insureproject.activities.PersonalDetails;
import insure.sise.grupo7.insureproject.activities.MyClaimActivity;

import static androidx.work.WorkRequest.MIN_BACKOFF_MILLIS;


public class OfflineAlertDialogHelper {
    public static void buildAlert(Context context, String activity){
        final Context _context = context;
        final String _activity = activity;
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Offline mode",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("ConnState", "Offline");
                OneTimeWorkRequest retryConnectionWork =
                        new OneTimeWorkRequest.Builder(RetryConnectionWorker.class)
                                .setBackoffCriteria(BackoffPolicy.LINEAR, MIN_BACKOFF_MILLIS, TimeUnit.MILLISECONDS)
                                .build();
                WorkManager.getInstance().enqueue(retryConnectionWork);
                switch (_activity) {
                    case ("LOGIN"):
                        _context.startActivity(new Intent(_context, HomePageActivity.class));
                        break;
                    case ("CLAIMDETAILS"):
                        ((ClaimDetails)_context).readFromFile();
                        break;
                    case ("CLAIMSUBMIT"):

                        break;
                    case ("CLAIMSUBMIT_PLATES"):
                        ((ClaimSubmitActivity)_context).readFromFile();
                        break;
                    case ("MYCLAIMS"):
                        ((MyClaimActivity)_context).readFromFile();
                        break;
                    case ("PERSONALDETAILS"):
                        ((PersonalDetails)_context).readFromFile();
                        break;
                    case ("CLAIMCHAT"):
                        Intent intent = new Intent(_context, HomePageActivity.class);
                        _context.startActivity(intent);
                        break;
                }
            }
        });
        alertDialog.setButton(Dialog.BUTTON_NEUTRAL, "Try to connect",new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.setTitle("Connection to the server has failed.");
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setMessage("You can retry to connect or proceed in Offline Mode. In Offline mode, you can still use the application and requests will be executed as soon as there is a connection available.");
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try {
                    new PingServer().execute().get(4000, TimeUnit.MILLISECONDS);
                    if ( !GlobalState.getOfflineState() ) {
                        Log.d("ConnState", "Online");
                        alertDialog.dismiss();
                        ConnectionReestablishedAlertDialog.buildAlert(_context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

