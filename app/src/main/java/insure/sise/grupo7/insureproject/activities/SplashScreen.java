package insure.sise.grupo7.insureproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import androidx.work.WorkManager;

/**
 * Created by joaopedsantos on 18-07-2018.
 */

public class SplashScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(SplashScreen.this, Login.class);
        startActivity(intent);
        finish();
        WorkManager.getInstance().cancelAllWorkByTag("insure.sise.grupo7.insureproject.Workers.NewMessageWorker");
    }
}
