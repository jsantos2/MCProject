package insure.sise.grupo7.insureproject.AlertDialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

public class IncorrectPasswordAlertDialogHelper {
    public static void buildAlert(Context context){
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Close",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("Cenas", "cenas");
            }
        });
        alertDialog.setTitle("Incorrect username and/or password");
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setMessage("Please try again.");
        alertDialog.show();
    }
}