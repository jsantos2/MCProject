package insure.sise.grupo7.insureproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import insure.sise.grupo7.insureproject.AlertDialogs.NeverLoggedInAlertDialog;
import insure.sise.grupo7.insureproject.AlertDialogs.OfflineAlertDialogHelper;
import insure.sise.grupo7.insureproject.adapters.ClaimListAdapter;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.WebServer.WSListClaimsCallTask;
import insure.sise.grupo7.insureproject.datamodel.ClaimItemStatus;

public class MyClaimActivity extends BaseActivity {
    List<ClaimItemStatus> claimItemStatusList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_claim);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        super.prepareMenu(toolbar, "MyClaim");

        getClaimList();
    }

    public void listViewHandler() {
        ListView mListView = (ListView) findViewById(R.id.listView);
        final ClaimListAdapter claimsAdapter = new ClaimListAdapter(this, R.layout.adapter_view_layout, claimItemStatusList);
        mListView.setAdapter(claimsAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ClaimItemStatus item = claimsAdapter.getItem(position);
                GlobalState.setClaimId(item.getId());
                goToClaimDetails(item.getId());
            }
        });
    }

    public void getClaimList() {
        if ( !GlobalState.getOfflineState() ) {
            new WSListClaimsCallTask(this, this, GlobalState.getSessionId()).execute();
        } else {
            readFromFile();
        }
    }

    public void readFromFile() {
        try {
            String _username = GlobalState.getUsername();
            List<ClaimItemStatus> claimItemStatuses = (List<ClaimItemStatus>) InternalStorage.readObject(this, "ClaimList" + "_username_" + _username);
            if ( claimItemStatuses == null ) {
                neverLoggedInOffline();
            } else {
                this.claimItemStatusList = claimItemStatuses;
                listViewHandler();
            }
        } catch (IOException |ClassNotFoundException e) {
            Log.e("Exception", e.getMessage());
        }
    }

    public void goToClaimDetails(int claimId) {
        Intent intent = new Intent(getBaseContext(), ClaimDetails.class);
        intent.putExtra("SELECTED_CLAIM_ID", claimId);
        startActivity(intent);
    }

    public void receiveClaimList(List<ClaimItemStatus> claimItemList) {
        this.claimItemStatusList = claimItemList;
    }

    public void neverLoggedInOffline() {
        NeverLoggedInAlertDialog.buildAlert(this, "MYCLAIMS");
    }

    public void offlineAlertDialog() {
        GlobalState.enableOfflineMode();
        OfflineAlertDialogHelper.buildAlert(this, "MYCLAIMS");
    }
}
