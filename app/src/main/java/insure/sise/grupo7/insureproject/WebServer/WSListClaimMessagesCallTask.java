package insure.sise.grupo7.insureproject.WebServer;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.activities.ClaimMessages;
import insure.sise.grupo7.insureproject.datamodel.ClaimItem;
import insure.sise.grupo7.insureproject.datamodel.ClaimMessage;

public class WSListClaimMessagesCallTask extends AsyncTask<Void, String, String> {
    public final static String TAG = "ListClaimMessages";
    private int insureMessagesCounter = 0;
    private List<ClaimMessage> claimMlist;
    private Context _context;
    public WSListClaimMessagesCallTask(Context context){
        _context = context;
    }

    @Override
    protected String doInBackground(Void... params) {
        String m = "empty array";
        publishProgress("Testing method call listClaimMessages...");
        try {
            int claimId = GlobalState.getClaimId();
            List<ClaimMessage> claimMessageList = WSHelper.listClaimMessages(GlobalState.getSessionId(), claimId);
            claimMlist = claimMessageList;
            if (claimMessageList != null) {
                m = claimMessageList.size() > 0 ? "" : "empty array";
                for (ClaimMessage cm : claimMessageList ) {
                    if (cm.getSender().equals("AutoInSure")){
                        insureMessagesCounter++;
                    }
                    m += " (Sender:" + cm.getSender() +
                            ", Date: " + cm.getDate() +
                            ", Message: " + cm.getMessage() + ")";
                }
                GlobalState.setMessagesCounter(insureMessagesCounter);
                Log.d(TAG, "List claim messages for claimId " + claimId + " result =>" + m);
            } else {
                Log.d(TAG, "List claim messages for claimId " + claimId + " result => null.");
            }
            publishProgress("ok.\n");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
        }

        return m;
    }

    protected void onPostExecute(String m) {
        if (claimMlist == null) {
            ((ClaimMessages)_context).offlineAlertDialog();
        } else {
            ((ClaimMessages)_context).setMessage(m);
        }
    }
}
