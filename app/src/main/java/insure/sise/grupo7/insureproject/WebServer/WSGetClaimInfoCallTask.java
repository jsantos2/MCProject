package insure.sise.grupo7.insureproject.WebServer;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.activities.ClaimDetails;
import insure.sise.grupo7.insureproject.activities.HomePageActivity;
import insure.sise.grupo7.insureproject.datamodel.ClaimRecord;

public class WSGetClaimInfoCallTask extends AsyncTask<Void, String, ClaimRecord> {
    public final static String TAG = "GetClaimInfoTask";
    public int _sessionId;
    public int _claimId;
    public Context _context;
    public ClaimDetails _claimDetails;
    public ClaimRecord _claimRecord;

    public WSGetClaimInfoCallTask(ClaimDetails claimDetails, Context context, int sessionId, int claimId) {
        _sessionId = sessionId;
        _claimId = claimId;
        _context = context;
        _claimDetails = claimDetails;
    }

    @Override
    protected ClaimRecord doInBackground(Void... params) {
        _claimRecord = null;
        publishProgress("Testing method call getClaimInfo...");
        try {
            ClaimRecord claimRecord = WSHelper.getClaimInfo(_sessionId, _claimId);
            _claimRecord = claimRecord;
            String _username = GlobalState.getUsername();
            if (claimRecord != null) {
                InternalStorage.writeObject(_context, "ClaimRecordInfo" + "_claim" + _claimId + "_username_" + _username, claimRecord);
                Log.d(TAG, "Get Claim Info result claimId " + _claimId + " => " + claimRecord.toString());
            } else {
                Log.d(TAG, "Get Claim Info result claimId " + _claimId + " => null.");
            }
            publishProgress("ok.\n");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
        }
        return _claimRecord;
    }


    protected void onPostExecute(ClaimRecord claimRecord) {
        if (claimRecord != null ) {
            _claimDetails.receiveClaimRecord(claimRecord);
            // build successful task alert dialog
        }
        else {
            _claimDetails.offlineAlertDialog();
        }
    }
}
