package insure.sise.grupo7.insureproject.Workers;

import android.util.Log;

import androidx.work.Worker;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.WebServer.WSHelper;

public class SubmitNewClaimWorker extends Worker {

    public Worker.Result doWork() {
        String TAG = "SubmitNewClaimWorker";
        Log.d(TAG, "Submit new claim worker is working...");
        try {
            int _sessionId = GlobalState.getWorkerSessionId(); //pass data in the right way
            String claimTitle = GlobalState.getClaimTitle();
            String occurenceDate = GlobalState.getOccurenceDate();
            String plate = GlobalState.getPlate();
            String claimDescription = GlobalState.getClaimDescription();
            boolean result = WSHelper.submitNewClaim(_sessionId, claimTitle, occurenceDate, plate, claimDescription);
            if ( result ) {
                Log.d(TAG, "Claim submission successful.");
                return Result.SUCCESS;
            }
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
        Log.d(TAG,"Claim submission failed, retrying...");
        return Result.RETRY;
    }
}
