package insure.sise.grupo7.insureproject.Workers;

import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import java.util.Random;

import androidx.work.Worker;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.NotificationHelper;
import insure.sise.grupo7.insureproject.WebServer.WSHelper;

public class LoginWorker extends Worker {

    public Worker.Result doWork() {
        String TAG = "LoginWorker";
        Log.d(TAG, "Login worker is working");
        try {
            String _username = GlobalState.getUsername();
            String _password = GlobalState.getPassword();
            int sessionId = WSHelper.login(_username,_password);
            if ( sessionId > 0 ) {
                Log.d(TAG,"Successful login.");
                GlobalState.setWorkerSessionId(sessionId);
                return Result.SUCCESS;
            }
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
        Log.d(TAG,"Login failed, retrying...");
        return Result.RETRY;
    }
}
