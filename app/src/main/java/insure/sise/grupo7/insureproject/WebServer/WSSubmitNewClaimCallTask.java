package insure.sise.grupo7.insureproject.WebServer;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.util.Log;

import insure.sise.grupo7.insureproject.AlertDialogs.SubmissionSuccessfulAlertDialog;
import insure.sise.grupo7.insureproject.activities.ClaimSubmitActivity;


public class WSSubmitNewClaimCallTask extends AsyncTask<Void, String, Boolean> {
    public final static String TAG = "SubmitNewClaimTask";
    private int _sessionId;
    private String _claimTitle;
    private String _occurenceDate;
    private String _plate;
    private String _claimDescription;
    private ClaimSubmitActivity _claimSubmitActivity;
    private AlertDialog _builder;
    private boolean _r;

    public WSSubmitNewClaimCallTask(ClaimSubmitActivity activity, AlertDialog builder, int sessionId, String claimTitle, String occurrenceDate, String plate, String claimDescription) {
        _sessionId = sessionId;
        _claimTitle = claimTitle;
        _occurenceDate = occurrenceDate;
        _plate = plate;
        _claimDescription = claimDescription;
        _claimSubmitActivity = activity;
        _builder = builder;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        publishProgress("Testing method call submitNewClaim...");
        try {
            boolean r = WSHelper.submitNewClaim(_sessionId, _claimTitle, _occurenceDate, _plate, _claimDescription);
            _r = r;
            Log.d(TAG, "Submit new claim result => " + r);
            publishProgress("ok.\n");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
        }

        return _r;
    }

    protected void onPostExecute(Boolean r) {
        _builder.dismiss();
        if (r) {
            SubmissionSuccessfulAlertDialog.buildAlert(_claimSubmitActivity, _builder,"SUBMIT_CLAIM");
        }
        else {
            _claimSubmitActivity.offlineAlertDialogSubmit();
        }
    }
}