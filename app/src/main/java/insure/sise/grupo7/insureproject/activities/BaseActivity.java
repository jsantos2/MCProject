package insure.sise.grupo7.insureproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Toast;

import insure.sise.grupo7.insureproject.R;

/**
 * Created by joaopedsantos on 17-07-2018.
 */

public class BaseActivity extends AppCompatActivity {

    protected Toolbar _toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.myAccountId:
                this.startActivity(new Intent(this, PersonalDetails.class));
                break;
            case R.id.myClaimsId:
                this.startActivity(new Intent(this, MyClaimActivity.class));
                break;
            case R.id.newClaimId:
                this.startActivity(new Intent(this, ClaimSubmitActivity.class));
                break;
            case R.id.logOutId:
                Intent intent = new Intent(this, Login.class);
                this.startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void prepareMenu(Toolbar toolbar, String page){
        _toolbar = toolbar;
        setSupportActionBar(_toolbar);
        getSupportActionBar().setLogo(R.drawable.insurelogo);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        switch(page) {
            case "ClaimDetails":
                getSupportActionBar().setTitle("    Claim Details");
                break;
            case "ClaimSubmit":
                getSupportActionBar().setTitle("    New Claim");
                break;
            case "ContactUs":
                getSupportActionBar().setTitle("    Contact Us");
                break;
            case "HomePage":
                getSupportActionBar().setTitle("    Home Page");
                break;
            case "MyClaim":
                getSupportActionBar().setTitle("    My Claims");
                break;
            case "PersonalDetails":
                getSupportActionBar().setTitle("    My Account");
                break;
            case "ClaimMessages":
                getSupportActionBar().setTitle("    Insure Chat");
                break;

        }
    }
}
