package insure.sise.grupo7.insureproject.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import insure.sise.grupo7.insureproject.AlertDialogs.IncorrectPasswordAlertDialogHelper;
import insure.sise.grupo7.insureproject.AlertDialogs.NeverLoggedInAlertDialog;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.AlertDialogs.OfflineAlertDialogHelper;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.WebServer.WSLoginCallTask;


public class Login extends AppCompatActivity{


    @SuppressLint("ClickableViewAccessibility")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.drawable.insurelogo);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("                                       Log In");

        Button button = findViewById(R.id.button1);
        Typeface font = Typeface.createFromAsset(getAssets(), "tahoma.ttf");
        button.setTypeface(font);
        TextView noAccountText = findViewById(R.id.noAccountTextView);
        noAccountText.setTypeface(font);

    }

    public void login(View v) {

        EditText emailField = (EditText)findViewById(R.id.editText);
        String emailString =  emailField.getText().toString();
        EditText passwordField = (EditText)findViewById(R.id.editText2);
        String passwordString =  passwordField.getText().toString();

        AlertDialog builder = new AlertDialog.Builder(this).create();
        View view = LayoutInflater.from(this).inflate(R.layout.alertdialog_authentication, null);
        TextView title = (TextView) view.findViewById(R.id.title);
        builder.setCancelable(false);
        builder.setView(view);
        builder.show();

        new WSLoginCallTask(this,Login.this, builder, emailString, passwordString).execute();
    }

    public void offlineAlertDialog() {
        GlobalState.enableOfflineMode();
        OfflineAlertDialogHelper.buildAlert(this, "LOGIN");
    }

    public void incorrectPasswordDialog() {
        IncorrectPasswordAlertDialogHelper.buildAlert(this);
    }

    public void neverLoggedInOffline() {
        NeverLoggedInAlertDialog.buildAlert(this, "LOGIN");
    }

}
