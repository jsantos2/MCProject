package insure.sise.grupo7.insureproject.WebServer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import androidx.work.BackoffPolicy;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.Workers.NewMessageWorker;
import insure.sise.grupo7.insureproject.activities.ClaimMessages;
import insure.sise.grupo7.insureproject.activities.HomePageActivity;
import insure.sise.grupo7.insureproject.activities.Login;
import insure.sise.grupo7.insureproject.datamodel.LoginCreds;

public class WSLoginCallTask extends AsyncTask<Void, String, Integer> {
    public final static String TAG = "LoginTask";
    int sessionId = -1;
    private AlertDialog _builder;
    private Context _context;
    private String _username;
    private String _password;
    private Login _loginActivity;
    public WSLoginCallTask(Login loginActivity, Context activityContext, AlertDialog builder, String username, String password) {
        _builder = builder;
        _context = activityContext.getApplicationContext();
        _username = username;
        _password = password;
        _loginActivity = loginActivity;
    }
    @Override
    protected Integer doInBackground(Void... params) {
        publishProgress("Testing method call login right...");
        try{
            sessionId = WSHelper.login(_username,_password);        // exists and password correct
            Log.d(TAG, "Login result => " + sessionId);
            publishProgress("ok.\n");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            publishProgress("failed.\n");
        }
        return sessionId;
    }

    @Override
    protected void onPostExecute(Integer sessionId) {
        GlobalState.setUsernameAndPassword(_username, _password);
        _builder.dismiss();

        WorkManager.getInstance().cancelAllWorkByTag("insure.sise.grupo7.insureproject.Workers.NewMessageWorker");
        PeriodicWorkRequest.Builder myWorkBuilder =
                new PeriodicWorkRequest.Builder(NewMessageWorker.class, 10,
                        TimeUnit.SECONDS).setBackoffCriteria(BackoffPolicy.LINEAR, 15, TimeUnit.SECONDS);
        PeriodicWorkRequest myWork = myWorkBuilder.build();
        WorkManager.getInstance()
                .enqueueUniquePeriodicWork("NewMessageWorker", ExistingPeriodicWorkPolicy.REPLACE, myWork);

        if (sessionId > 0) {
            LoginCreds validLoginCreds = new LoginCreds(_username, _password);
            GlobalState.setLoggedOutState(false);
            try {
                InternalStorage.writeObject(_context, "loginCreds" + "_username_" + _username, validLoginCreds);
            } catch (IOException e) {
                e.printStackTrace();
            }
            GlobalState.setSessionId(sessionId);
            _context.startActivity(new Intent(_context, HomePageActivity.class));
        }
        else if (sessionId == 0) {
            _loginActivity.incorrectPasswordDialog();
        }
        else {
            try {
                LoginCreds previousValidCreds = (LoginCreds) InternalStorage.readObject(_context, "loginCreds" + "_username_" + _username);
                String _previousUsername = previousValidCreds.get_username();
                String _previousPassword = previousValidCreds.getPassword();
            if (_previousUsername.equals(_username) && _previousPassword.equals(_password)) {
                _loginActivity.offlineAlertDialog();
            }
            } catch (IOException|ClassNotFoundException e) {
                _loginActivity.neverLoggedInOffline();
                e.printStackTrace();
            }
        }
    }
}
