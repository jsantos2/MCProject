package insure.sise.grupo7.insureproject.AlertDialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import insure.sise.grupo7.insureproject.activities.HomePageActivity;
import insure.sise.grupo7.insureproject.activities.MyClaimActivity;

public class NeverLoggedInAlertDialog {
    private static String _activity;
    private static Context _context;
    public static void buildAlert(Context context, String activity) {
        _activity = activity;
        _context = context;
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch(_activity) {
                    case "CLAIM_SUBMIT":
                        _context.startActivity(new Intent(_context, HomePageActivity.class));
                        break;
                    case "LOGIN":
                        break;
                    case "GET_PERSONAL_INFO":
                        _context.startActivity(new Intent(_context, HomePageActivity.class));
                        break;
                    case "CLAIMDETAILS":
                        _context.startActivity(new Intent(_context, MyClaimActivity.class));
                        break;
                    case "MYCLAIMS":
                        _context.startActivity(new Intent(_context, HomePageActivity.class));
                        break;
                }
            }
        });
        alertDialog.setTitle("Server is unreachable");
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setMessage("Offline mode cannot start because the user never logged in or never online to this part of the app. Please try again later.");
        alertDialog.show();
    }
}