package insure.sise.grupo7.insureproject.Workers;

import android.app.Notification;
import android.util.Log;

import java.util.Random;

import androidx.work.Worker;
import insure.sise.grupo7.insureproject.AlertDialogs.IncorrectPasswordAlertDialogHelper;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.NotificationHelper;
import insure.sise.grupo7.insureproject.WebServer.WSHelper;

public class RetryConnectionWorker extends Worker{
    private int sessionId;
    @Override
    public Worker.Result doWork() {
        if ( !GlobalState.getOfflineState() ) {
            return Result.SUCCESS;
        }
        NotificationHelper helper = new NotificationHelper(getApplicationContext());
        String TAG = "RetryWorker";
        Log.d(TAG, "Retry worker is working");
        String r = "";
        try {
            r = WSHelper.hello("John Doe");
            if ( !r.isEmpty() ) {
                GlobalState.disableOfflineMode();
                Notification.Builder builder = helper.getInsureChannelNotification("Insure Notification", "Connection reestablished.");
                helper.getManager().notify(new Random().nextInt(), builder.build());
                Log.d("RetryWorker:","success!");
                sessionId = WSHelper.login(GlobalState.getUsername(), GlobalState.getPassword());
                if (sessionId > 0) {
                    GlobalState.setSessionId(sessionId);
                } else {
                    builder = helper.getInsureChannelNotification("Insure Notification", "Wrong username and/or password.");
                    helper.getManager().notify(new Random().nextInt(), builder.build());
                }

                return Result.SUCCESS;
            }
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
        Log.d("RetryWorker:","Conection failed, retrying...");
        return Result.RETRY;
    }
}
