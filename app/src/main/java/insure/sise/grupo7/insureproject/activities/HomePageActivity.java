package insure.sise.grupo7.insureproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import insure.sise.grupo7.insureproject.R;

public class HomePageActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        super.prepareMenu(toolbar, "HomePage");

    }


    public void goToMyAccount(View view) {
        this.startActivity(new Intent(this, PersonalDetails.class));
    }

    public void goToNewClaimSubmission(View view) {
        this.startActivity(new Intent(this, ClaimSubmitActivity.class));
    }

    public void goToMyClaims(View view) {
        this.startActivity(new Intent(this, MyClaimActivity.class));
    }

    public void goToContactUs(View view) {
        this.startActivity(new Intent(this, ContactUs.class));
    }
}
