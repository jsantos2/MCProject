package insure.sise.grupo7.insureproject;

import android.app.Application;
import android.content.Context;

import java.util.concurrent.TimeUnit;

import androidx.work.BackoffPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkContinuation;
import androidx.work.WorkManager;
import insure.sise.grupo7.insureproject.Workers.LoginWorker;
import insure.sise.grupo7.insureproject.Workers.LogoutWorker;
import insure.sise.grupo7.insureproject.Workers.SubmitNewClaimWorker;

public class GlobalState extends Application {

    private static String _username;
    private static String _password;
    private static int _workerSessionId;
    private static int _sessionId;
    private static String claimTitle;
    private static String occurenceDate;
    private static String plate;
    private static String claimDescription;
    private static String message;
    private static WorkContinuation _workContinuation;
    private static int messagesCounter = 0;
    private static Context context;
    private static boolean destroyMessageWorker = false;

    public static boolean isLoggedOutState() {
        return loggedOutState;
    }
    public static void setLoggedOutState(boolean loggedOutState) {
        GlobalState.loggedOutState = loggedOutState;
    }
    private static boolean loggedOutState = false;
    public GlobalState () {
    }
    public static Context getContext(){ return context;}
    public static void setContext(Context ctx){ context=ctx;}
    public static void setMessagesCounter(int n){ messagesCounter = n;}
    public static int getMessagesCounter(){return messagesCounter;}
    public static  boolean getDestroyMessageWorker() {return destroyMessageWorker;}
    public static void setDestroyMessageWorker(boolean bool){destroyMessageWorker = bool; }
    public static String getMessage() {
        return message;
    }
    public static void setMessage(String message) {
        GlobalState.message = message;
    }
    public static int getClaimId() {
        return claimId;
    }
    public static void setClaimId(int claimId) {
        GlobalState.claimId = claimId;
    }
    private static int claimId;
    public static String getClaimTitle() {
        return claimTitle;
    }
    public static void setClaimTitle(String claimTitle) {
        GlobalState.claimTitle = claimTitle;
    }
    public static String getOccurenceDate() {
        return occurenceDate;
    }
    public static void setOccurenceDate(String occurenceDate) {
        GlobalState.occurenceDate = occurenceDate;
    }
    public static void setSessionId(int sessionId) {
        _sessionId = sessionId;
    }
    public static int getSessionId() {
        return _sessionId;
    }
    public static String getPlate() {
        return plate;
    }
    public static void setPlate(String plate) {
        GlobalState.plate = plate;
    }
    public static String getClaimDescription() {
        return claimDescription;
    }
    public static void setClaimDescription(String claimDescription) {
        GlobalState.claimDescription = claimDescription;
    }
    public static void setUsernameAndPassword(String username, String password) {
        _username = username;
        _password = password;
    }
    public static void setWorkerSessionId(int sessionId) {
        _workerSessionId = sessionId;
    }
    public static int getWorkerSessionId() {
        return _workerSessionId;
    }
    public static String getUsername() {
        return _username;
    }
    public static String getPassword() {
    return _password;
    }
    private static boolean offlineMode = false;
    public static void enableOfflineMode(){
        offlineMode = true;
    }
    public static void disableOfflineMode() {
        offlineMode = false;
    }
    public static boolean getOfflineState() {
        return offlineMode;
    }

    public static void startWorkManager(String type) {
        OneTimeWorkRequest loginWork =
                new OneTimeWorkRequest.Builder(LoginWorker.class)
                        .setBackoffCriteria(BackoffPolicy.LINEAR, 1, TimeUnit.MINUTES)
                        .build();
        _workContinuation = WorkManager.getInstance().beginUniqueWork("offlineWork", ExistingWorkPolicy.KEEP, loginWork);
            addSubmitClaimWork();
            startWork();
    }

    private static void addSubmitClaimWork() {
        OneTimeWorkRequest genericWork =
                new OneTimeWorkRequest.Builder(SubmitNewClaimWorker.class)
                        .build();
        _workContinuation = _workContinuation.then(genericWork);
    }

      private static void startWork() {
        OneTimeWorkRequest logoutWork =
                new OneTimeWorkRequest.Builder(LogoutWorker.class)
                        .build();
        _workContinuation = _workContinuation.then(logoutWork);
        _workContinuation.enqueue();
    }
}
