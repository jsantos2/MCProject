package insure.sise.grupo7.insureproject.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;

import insure.sise.grupo7.insureproject.AlertDialogs.NeverLoggedInAlertDialog;
import insure.sise.grupo7.insureproject.AlertDialogs.OfflineAlertDialogHelper;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.WebServer.WSGetCustomerInfoCallTask;
import insure.sise.grupo7.insureproject.datamodel.Customer;

public class PersonalDetails extends BaseActivity {

    Customer _customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        super.prepareMenu(toolbar, "PersonalDetails");

        getPersonalDetails();
    }

    public void receiveCustomerDetails(Customer customer) {
        this._customer = customer;
        TextView nameFieldLower = (TextView) this.findViewById(R.id.nameFieldPersonalDetails);
        nameFieldLower.setText(customer.getName());
        TextView nameField = (TextView) this.findViewById(R.id.introName);
        nameField.setText("Hi, " + customer.getName() + "!");
        TextView addressField = (TextView) this.findViewById(R.id.addressText);
        addressField.setText(customer.getAddress());
        TextView nifField = (TextView) this.findViewById(R.id.nifNumber);
        nifField.setText(String.valueOf(customer.getFiscalNumber()));
        TextView dateOfBirthField = (TextView) this.findViewById(R.id.dateOfBirthText);
        dateOfBirthField.setText(customer.getDateOfBirth());
        TextView policyField = (TextView) this.findViewById(R.id.claimTitle);
        policyField.setText(String.valueOf(customer.getPolicyNumber()));
    }

    public void offlineAlertDialog() {
        GlobalState.enableOfflineMode();
        OfflineAlertDialogHelper.buildAlert(this, "PERSONALDETAILS");
    }

    public void getPersonalDetails() {
        if ( !GlobalState.getOfflineState() ) {
            new WSGetCustomerInfoCallTask(this, this, GlobalState.getSessionId()).execute();
        }
        else {
            readFromFile();
        }
        }

    public void readFromFile() {
        try {
            String _username = GlobalState.getUsername();
            Customer customer = (Customer) InternalStorage.readObject(this, "PersonalDataInfo" + "_username_" + _username);
            if (customer == null) {
                neverLoggedInOffline();
            } else {
                TextView nameFieldLower = (TextView) this.findViewById(R.id.nameFieldPersonalDetails);
                nameFieldLower.setText(customer.getName());
                TextView nameField = (TextView) this.findViewById(R.id.introName);
                nameField.setText("Hi, " + customer.getName() + "!");
                TextView addressField = (TextView) this.findViewById(R.id.addressText);
                addressField.setText(customer.getAddress());
                TextView nifField = (TextView) this.findViewById(R.id.nifNumber);
                nifField.setText(String.valueOf(customer.getFiscalNumber()));
                TextView dateOfBirthField = (TextView) this.findViewById(R.id.dateOfBirthText);
                dateOfBirthField.setText(customer.getDateOfBirth());
                TextView policyField = (TextView) this.findViewById(R.id.claimTitle);
                policyField.setText(String.valueOf(customer.getPolicyNumber()));
            }
            } catch(IOException | ClassNotFoundException e){
                Log.e("Exception", e.getMessage());
                neverLoggedInOffline();
            }
    }


    public void neverLoggedInOffline() {
        NeverLoggedInAlertDialog.buildAlert(this, "GET_PERSONAL_INFO");
    }
}
