package insure.sise.grupo7.insureproject.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import insure.sise.grupo7.insureproject.AlertDialogs.NeverLoggedInAlertDialog;
import insure.sise.grupo7.insureproject.AlertDialogs.OfflineAlertDialogHelper;
import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.InternalStorage;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.WebServer.WSGetClaimInfoCallTask;
import insure.sise.grupo7.insureproject.datamodel.ClaimRecord;

public class ClaimDetails extends BaseActivity {

    private ClaimRecord _claimRecord;
    private int _claimId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        super.prepareMenu(toolbar, "ClaimDetails");


        _claimId = getIntent().getIntExtra("SELECTED_CLAIM_ID", 0);
        getClaimDetails();
    }

    public void goToClaimChat(View view) {
        Intent intent = new Intent(this, ClaimMessages.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(intent);
    }

    public void getClaimDetails() {
        if ( !GlobalState.getOfflineState() ) {
            new WSGetClaimInfoCallTask(this, this, GlobalState.getSessionId(), _claimId).execute();
        }
        else {
            readFromFile();
        }
    }

    public void readFromFile() {
        try {
            String _username = GlobalState.getUsername();
            ClaimRecord claimRecord = (ClaimRecord) InternalStorage.readObject(this, "ClaimRecordInfo" + "_claim" + _claimId + "_username_" + _username);
            if ( claimRecord == null ) {
                neverLoggedInOffline();
            } else {
                receiveClaimRecord(claimRecord);
            }
        } catch (IOException |ClassNotFoundException e) {
            Log.e("Exception", e.getMessage());
        }
    }

    public void offlineAlertDialog() {
        GlobalState.enableOfflineMode();
        OfflineAlertDialogHelper.buildAlert(this, "CLAIMDETAILS");
    }

    public void receiveClaimRecord(ClaimRecord claimRecord) {
        this._claimRecord = claimRecord;
        TextView claimId = (TextView) this.findViewById(R.id.introName);
        claimId.setText(String.format("Claim %s", _claimRecord.getId()));
        TextView claimTitle = (TextView) this.findViewById(R.id.claimTitle);
        claimTitle.setText(_claimRecord.getTitle());
        TextView claimDate = (TextView) this.findViewById(R.id.claimDate);
        claimDate.setText(_claimRecord.getOccurrenceDate());
        TextView claimPlate = (TextView) this.findViewById(R.id.claimPlate);
        claimPlate.setText(_claimRecord.getPlate());
        TextView claimDescription = (TextView) this.findViewById(R.id.claimDescription);
        claimDescription.setText(_claimRecord.getDescription());
        TextView claimStatus = (TextView) this.findViewById(R.id.claimStatus);
        claimStatus.setText(_claimRecord.getStatus().toUpperCase());
    }

    public void neverLoggedInOffline() {
        NeverLoggedInAlertDialog.buildAlert(this, "CLAIMDETAILS");
    }
}
