package insure.sise.grupo7.insureproject.AlertDialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import insure.sise.grupo7.insureproject.GlobalState;
import insure.sise.grupo7.insureproject.R;
import insure.sise.grupo7.insureproject.activities.HomePageActivity;

public class SubmissionSuccessfulAlertDialog {

    private static Context _context;

    public static void buildAlert(Context context, AlertDialog builder, String activity){
        _context = context;
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Close",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(_context, HomePageActivity.class);
                _context.startActivity(intent);
            }
        });
        alertDialog.setTitle("Success!");
        alertDialog.setIcon(R.drawable.ok_check);
        alertDialog.setCanceledOnTouchOutside(false);
        switch (activity) {
            case "SUBMIT_CLAIM":
                if (!GlobalState.getOfflineState()) {
                    alertDialog.setMessage("New claim submitted successfully.");
                    builder.dismiss();
                } else {
                    alertDialog.setMessage("Offline request successful. You will be informed when the connection is reestablished and the claim submission is made to InSure.");
                    builder.dismiss();
                }
                break;
        }
        alertDialog.show();
    }
}
